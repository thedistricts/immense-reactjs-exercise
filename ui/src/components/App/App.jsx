import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import Menu from '../Menu';
import SideBar from '../SideBar';
import Map from '../Map';
import './App.css';

const client = new ApolloClient({
  // uri: 'http://localhost:3005/dev/graphql',
  uri: 'https://roe7qa6orl.execute-api.us-east-1.amazonaws.com/dev/graphql',
  cache: new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <Menu />
        <SideBar />
        <Map />
      </div>
    </ApolloProvider>
  );
}

export default App;
