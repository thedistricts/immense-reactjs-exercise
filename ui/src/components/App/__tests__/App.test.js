import { render, screen } from "@testing-library/react";
import App from "../App";

describe("App", () => {
  test("renders correctly", () => {
    const { container } = render(<App />);
    expect(container).toMatchSnapshot();
  });

  test("renders basic title", () => {
    render(<App />);
    const titleElement = screen.getByText(/Fleet Management/i);
    expect(titleElement).toBeInTheDocument();
  });
});
