import T from 'prop-types';
import { useQuery, gql } from '@apollo/client';
import './Menu.css';
import car from './car.png';

const PLATFORM_VERSION = gql`
  query platform {
    version
  }
`;

export const Menu = ({ version }) => {
    return (
        <header className="Menu">
            <h1 className="Menu__title">
                <img src={car} className="Menu__icon" alt="vehicle icon" />
                Fleet Management {version && `v${version}`}
            </h1>
        </header>
    );
};

Menu.propTypes = {
    version: T.string
};

export const MenuWithData = () => {
    const { loading, error, data } = useQuery(PLATFORM_VERSION);
    if (loading || error) return <Menu />
    return <Menu version={data.version} />
};

export default MenuWithData;
