import { render, screen } from "@testing-library/react";
import { Menu, MenuWithData } from "../Menu";
import * as Apollo from '@apollo/client';

describe("Menu", () => {
    test("should render correctly", () => {
        const { container } = render(<Menu />);
        expect(container).toMatchSnapshot();
    });

    test("should render basic title", () => {
        render(<Menu />);
        const titleElement = screen.getByText(/Fleet Management/i);
        expect(titleElement).toBeInTheDocument();
    });

    test("should render title with version", () => {
        render(<Menu version='20.4.2' />);
        const titleElement = screen.getByText(/Fleet Management v20.4.2/i);
        expect(titleElement).toBeInTheDocument();
    });
});

describe("MenuWithData", () => {
    test("should renders title without version if loading", () => {
        jest.spyOn(Apollo, 'useQuery').mockImplementation(() => {
            return {
                loading: true,
                error: undefined,
                data: { version: '20.4.x' },
                refetch: jest.fn()
            }
        });
        render(<MenuWithData />);
        let titleElement = screen.queryByText(/Fleet Management v20.4.x/i);
        expect(titleElement).not.toBeInTheDocument();
        titleElement = screen.getByText(/Fleet Management/i);
        expect(titleElement).toBeInTheDocument();
    });

    test("should renders title without version if error", () => {
        jest.spyOn(Apollo, 'useQuery').mockImplementation(() => {
            return {
                loading: false,
                error: true,
                data: { version: '20.4.x' },
                refetch: jest.fn()
            }
        });
        render(<MenuWithData />);
        const titleElement = screen.getByText(/Fleet Management/i);
        expect(titleElement).toBeInTheDocument();
    });

    test("should render title with version", () => {
        jest.spyOn(Apollo, 'useQuery').mockImplementation(() => {
            return {
                loading: false,
                error: undefined,
                data: { version: '20.4.x' },
                refetch: jest.fn()
            }
        });
        render(<MenuWithData />);
        const titleElement = screen.getByText(/Fleet Management v20.4.x/i);
        expect(titleElement).toBeInTheDocument();
    });


});