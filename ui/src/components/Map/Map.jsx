import { MapContainer, TileLayer } from 'react-leaflet';
import DepotMarkers from './components/DepotMarkers';
import './Map.css';

const MAP = {
    PADDING: [50, 50],
    INIT_CENTER: [52.026108225348, -0.7416199999999984],
    ZOOM: 13,
};

export const Map = () => {
    return (
        <section className="Map">
            <MapContainer
                boundsOptions={{ padding: MAP.PADDING }}
                center={MAP.INIT_CENTER}
                zoom={MAP.ZOOM}
                scrollWheelZoom={false}
            >
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <DepotMarkers />
            </MapContainer>
        </section>
    );
}

export default Map;
