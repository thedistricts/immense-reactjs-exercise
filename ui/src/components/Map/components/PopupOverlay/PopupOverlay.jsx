import T from 'prop-types';
import { useQuery, gql } from '@apollo/client';
import L from 'leaflet';
import { Popup } from 'react-leaflet';
import './PopupOverlay.css';
import tick from './tick.svg';

const GET_DEPOT = gql`
    query get_depot($id: ID!) {
        depot(id: $id) {
            id
            name
            bays
            depot
            latitude
            longitude
            diesel
            petrol
            residential
            public
            fast
            rapid
        }
    }
`;

export const getTick = (isTicked) => {
    return isTicked
        ? <img src={tick} className="PopupOverlay__icon PopupOverlay__icon--tick" alt="Yes" />
        : <span className="PopupOverlay__icon PopupOverlay__icon--none text--hide">No</span>;
}

export const PopupOverlay = ({ depot }) => {
    if (!depot) return null;
    return (
        <Popup minWidth={320} offset={L.point(210, 228)}>
            <div className="PopupOverlay">
                <section className="PopupOverlay__section">
                    <span className="PopupOverlay__label PopupOverlay__full">Name</span>
                    <strong className="PopupOverlay--primary">{depot.name}</strong>
                </section>
                <section className="PopupOverlay__section PopupOverlay__section--fstart">
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Is it a depot?</span>
                        {getTick(depot.depot)}
                    </div>

                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Latitude</span>
                        <span className="PopupOverlay__value">{depot.latitude}</span>
                    </div>

                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Longitude</span>
                        <span className="PopupOverlay__value">{depot.longitude}</span>
                    </div>
                </section>

                <section className="PopupOverlay__section PopupOverlay__section--fstart PopupOverlay__section--minh">
                    <strong className="PopupOverlay__title PopupOverlay__full">Refuel</strong>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Deisel</span>
                        {getTick(depot.diesel)}
                    </div>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Petrol</span>
                        {getTick(depot.petrol)}
                    </div>
                </section>

                <section className="PopupOverlay__section PopupOverlay__section--minh">
                    <strong className="PopupOverlay__title PopupOverlay__full">Recharge</strong>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Residential</span>
                        {getTick(depot.residential)}
                    </div>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Public</span>
                        {getTick(depot.public)}
                    </div>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Fast</span>
                        {getTick(depot.fast)}
                    </div>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Rapid</span>
                        {getTick(depot.rapid)}
                    </div>
                    <div className="PopupOverlay__stat">
                        <span className="PopupOverlay__label">Bays</span>
                        <span className="PopupOverlay__value PopupOverlay__value--small">{depot.bays}</span>
                    </div>
                </section>
            </div>
        </Popup>)
}

PopupOverlay.propTypes = {
    depot: T.shape({
        id: T.string,
        name: T.string,
        bays: T.number,
        depot: T.bool,
        latitude: T.number,
        longitude: T.number,
        diesel: T.bool,
        petrol: T.bool,
        residential: T.bool,
        public: T.bool,
        fast: T.bool,
        rapid: T.bool,
    }),
}

export const PopupOverlayWithData = ({ id }) => {
    const { loading, error, data } = useQuery(GET_DEPOT, {
        variables: { id },
    });
    if (loading || error) return null
    return <PopupOverlay depot={data.depot} />
}

PopupOverlayWithData.propTypes = {
    id: T.string,
}

export default PopupOverlayWithData;