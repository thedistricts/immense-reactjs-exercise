import { render, screen } from "@testing-library/react";
import { PopupOverlay, PopupOverlayWithData, getTick } from "../PopupOverlay";
import * as Apollo from "@apollo/client";
import { Popup } from 'react-leaflet';

jest.mock("react-leaflet", () => {
    return {
        Popup: jest.fn()
    }
});

const depotMock = {
    __typename: "Depot",
    id: "D0:52:0",
    name: "D0",
    bays: 6,
    depot: true,
    latitude: 52.04309,
    longitude: -0.71796,
    diesel: false,
    petrol: true,
    residential: false,
    public: false,
    fast: false,
    rapid: false,
};

describe("PopupOverlay", () => {

    beforeEach(() => {
        Popup.mockImplementation(({ children }) => (
            <div>{children}</div>
        ))
    });

    test("should render correctly", () => {
        const { container } = render(<PopupOverlay depot={depotMock} />);
        expect(container).toMatchSnapshot();
    });

    test("should not render without depot", () => {
        const { container } = render(<PopupOverlay depot={null} />);
        expect(container).toMatchInlineSnapshot(`<div />`);
    });
});

describe("PopupOverlayWithData", () => {

    beforeEach(() => {
        Popup.mockImplementation(({ children }) => (
            <div>{children}</div>
        ))
    });

    test("should not render when loading", () => {
        jest.spyOn(Apollo, "useQuery").mockImplementation(() => {
            return {
                loading: true,
                error: undefined,
                data: { depot: null },
                refetch: jest.fn(),
            };
        });
        render(<PopupOverlayWithData id="D0:52:0" />);
        const titleElement = screen.queryByText(/D0/i);
        expect(titleElement).not.toBeInTheDocument();
    });

    test("should not render with error", () => {
        jest.spyOn(Apollo, "useQuery").mockImplementation(() => {
            return {
                loading: false,
                error: true,
                data: { depot: null },
                refetch: jest.fn(),
            };
        });
        render(<PopupOverlayWithData id="D0:52:0" />);
        const titleElement = screen.queryByText(/D0/i);
        expect(titleElement).not.toBeInTheDocument();
    });

    test("should render with data", () => {
        jest.spyOn(Apollo, "useQuery").mockImplementation(() => {
            return {
                loading: false,
                error: undefined,
                data: { depot: depotMock },
                refetch: jest.fn(),
            };
        });
        render(<PopupOverlayWithData id="D0:52:0" />);
        const titleElement = screen.getByText(/D0/i);
        expect(titleElement).toBeInTheDocument();
    });
});

describe("getTick", () => {
    test("should renders Yes if true", () => {
        expect(getTick(true)).toMatchSnapshot();
    });
    test("should renders No if false", () => {
        expect(getTick(false)).toMatchSnapshot();
    });
});
