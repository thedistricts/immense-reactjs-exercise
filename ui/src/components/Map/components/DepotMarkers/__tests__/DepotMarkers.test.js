import { render, screen } from "@testing-library/react";
import { DepotMarkers, DepotMarkersWithData } from "../DepotMarkers";
import { PopupOverlayWithData } from "../../PopupOverlay";
import * as Apollo from "@apollo/client";
import { Marker, useMap } from 'react-leaflet';

jest.mock("../../PopupOverlay");
jest.mock("react-leaflet", () => ({
    Marker: jest.fn(),
    useMap: jest.fn(),
}));

const depotsFlatMock = [{
    id: "D0:52:0",
    name: "D0",
    latitude: 52.04309,
    longitude: -0.71796,
}, {
    id: "D1:52:1",
        name: "D1",
    latitude: 52.04309,
        longitude: -0.71796,
}];

const depotsQueryMock = { "__typename": "DepotConnection", "edges": [{ "__typename": "DepotNode", "node": { "__typename": "Depot", "id": "D0:52:0", "name": "D0", "latitude": 52.04309, "longitude": -0.71796 } }, { "__typename": "DepotNode", "node": { "__typename": "Depot", "id": "D1:52:0", "name": "D1", "latitude": 52.00912, "longitude": -0.7426 } }, { "__typename": "DepotNode", "node": { "__typename": "Depot", "id": "D2:52:0", "name": "D2", "latitude": 52.03252, "longitude": -0.76528 } }] };

describe("DepotMarkers", () => {
    let fitBoundsMock;
    beforeEach(() => {
        fitBoundsMock = jest.fn();
        useMap.mockImplementation(() => ({
            fitBounds: fitBoundsMock
        }));
        Marker.mockImplementation(({ children }) => (
            <div data-testid="Marker">{children}</div>
        ));
        PopupOverlayWithData.mockImplementation(({ children, id }) => (
            <div data-testid="PopupOverlay">{id}{children}</div>
        ));
    });

    test("should render correctly", () => {
        const { container } = render(<DepotMarkers depots={depotsFlatMock} />);
        expect(container).toMatchSnapshot();
        const markers = screen.queryAllByTestId('Marker');
        expect(markers).toHaveLength(2);
    });

    test("should adjust map based on displayed markers", () => {
        render(<DepotMarkers depots={depotsFlatMock} />);
        expect(fitBoundsMock).toHaveBeenCalledWith({ "_southWest": { "lat": 52.04309, "lng": -0.71796 }, "_northEast": { "lat": 52.04309, "lng": -0.71796 } });
    });
});

describe("DepotMarkersWithData", () => {

    beforeEach(() => {
        useMap.mockImplementation(() => ({
            fitBounds: jest.fn()
        }));
        Marker.mockImplementation(({ children }) => (
            <div data-testid="Marker">{children}</div>
        ));
        PopupOverlayWithData.mockImplementation(({ children, id }) => (
            <div data-testid="PopupOverlay">{id}{children}</div>
        ));
    });

    test("should not render when loading", () => {
        jest.spyOn(Apollo, "useQuery").mockImplementation(() => {
            return {
                loading: true,
                error: undefined,
                data: { depot: null },
                refetch: jest.fn(),
            };
        });
        render(<DepotMarkersWithData />);
        const titleElement = screen.queryByText(/D0:52:0/i);
        expect(titleElement).not.toBeInTheDocument();
    });

    test("should not render with error", () => {
        jest.spyOn(Apollo, "useQuery").mockImplementation(() => {
            return {
                loading: false,
                error: true,
                data: { depot: null },
                refetch: jest.fn(),
            };
        });
        render(<DepotMarkersWithData />);
        const titleElement = screen.queryByText(/D0:52:0/i);
        expect(titleElement).not.toBeInTheDocument();
    });

    test("should render with data", () => {
        jest.spyOn(Apollo, "useQuery").mockImplementation(() => {
            return {
                loading: false,
                error: undefined,
                data: { depots: depotsQueryMock },
                refetch: jest.fn(),
            };
        });
        const { container } = render(<DepotMarkersWithData />);
        expect(container).toMatchSnapshot();
    });
});