import React from 'react';
import T from 'prop-types';
import { useQuery, gql } from '@apollo/client';
import L, { latLngBounds } from 'leaflet';
import { Marker, useMap } from 'react-leaflet';
import PopupOverlay from '../PopupOverlay';
import pin from './pin-depot.svg';

const GET_DEPOTS_LIST = gql`
  query get_depots {
    depots {
      edges {
        node {
            id
            name
            latitude
            longitude
        }
      }
    }
  }
`;

export const DepotMarkersWithData = () => {
    const { loading, error, data } = useQuery(GET_DEPOTS_LIST);
    if (loading || error) return null
    const depots = data.depots.edges.map(({ node }) => node);
    return <DepotMarkers depots={depots} />
}

export const DepotMarkers = ({ depots }) => {
    const map = useMap();
    const [bounds, setBounds] = React.useState();

    React.useEffect(() => {
        if (depots.length > 0) {
            let markerBounds = latLngBounds();
            depots.forEach(depot => {
                markerBounds.extend([depot.latitude, depot.longitude]);
            });
            setBounds(markerBounds);
        }
    }, [depots]);

    React.useEffect(() => {
        if (bounds) map.fitBounds(bounds);
    }, [bounds, map]);

    return depots.map(depot => {
        return (
            <Marker
                position={[depot.latitude, depot.longitude]}
                key={`${depot.id}`}
                icon={pinIcon}
            >

                <PopupOverlay id={depot.id} />
            </Marker>
        );
    })
};

const pinIcon = L.icon({
    iconUrl: pin,
    iconRetinaUrl: pin,
    shadowUrl: null,
    iconSize: [30, 40],
    shadowSize: [0, 0],
    iconAnchor: [15, 40],
    shadowAnchor: [15, 40],
    popupAnchor: [-3, -76]
});

DepotMarkers.defaultProps = {
    depots: [],
}

DepotMarkers.propTypes = {
    depots: T.arrayOf(
        T.shape({
            id: T.string,
            name: T.string,
            latitude: T.number,
            longitude: T.number,
        })
    )
};

export default DepotMarkersWithData;