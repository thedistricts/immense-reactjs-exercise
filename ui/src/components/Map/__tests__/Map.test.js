import { render } from "@testing-library/react";
import Map from "../Map";
import { DepotMarkersWithData } from "../components/DepotMarkers";
jest.mock("../components/DepotMarkers");

describe("Map", () => {
    beforeEach(() => {
        DepotMarkersWithData.mockImplementation(({ children }) => (
            <div>{children}</div>
        ))
    });
    test("renders correctly", () => {
        const { container } = render(<Map />);
        expect(container).toMatchSnapshot();
    });
});
