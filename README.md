# Immense ReactJS Exercise 

React App, Serverless GraphQL displaying GIS data for Depots.

_Author: [Dan Nisbet](mailto:hello@dannisbet.co.uk)_


## Live

-----

- [GraphQL API](https://roe7qa6orl.execute-api.us-east-1.amazonaws.com/dev/graphql) (AWS Lambda)
- [UI](https://elegant-raman-6d4024.netlify.app/)


## Local

-----

**API**

- `cd graphql`
- `yarn install`
- `yarn api:local`


**UI**

- `cd ui`
- `yarn install`
- `yarn ui:local`

**Tests**

- `yarn ui:test:unit`
- `yarn api:test:unit`

-----

**Prerequisites**

- Node 12.x
- Yarn


## Future Enhancements

-----

- menu vehicle icon should ideally be vector based (svg)
- custom map controls
- cypress E2E / intergration tests
- precache assets
- serverworker for offline fall back