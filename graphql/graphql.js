const { ApolloServer } = require('apollo-server-lambda');
const PlatformAPI = require('./src/datasources/platform.rest.api');
const { typeDefs, resolvers } = require('./src');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => {
        return {
            platformAPI: new PlatformAPI(),
        };
    },
    playground: {
        endpoint: '/dev/graphql'
    }
});

module.exports = {
    graphqlHandler: server.createHandler({
        cors: {
            origin: '*',
            credentials: true,
        },
    }),
};
