const { getDepots, getDepotById, transform, transformEdge, transformNode, DEPOTS_DATA, } = require('../depots');

describe('resolvers | depots | transforms', () => {
    test('should transform depots data', () => {
        expect(transform({ depots: DEPOTS_DATA })).toMatchSnapshot();
    });

    test('should transform edge', () => {
        expect(transformEdge(DEPOTS_DATA[0])).toMatchSnapshot();
    });

    test('should transform node', () => {
        expect(transformNode({ depot: DEPOTS_DATA[1]} )).toMatchSnapshot();
    });
});

describe('resolvers | depots | resolver', () => {
    test('should return depots', () => {
        expect(getDepots({ depots: DEPOTS_DATA })).toMatchSnapshot();
    });

    test('should return depots data', () => {
        const id = 'D2:52:0';
        const requestDepot = getDepotById(null, { id }, null, null);
        expect(requestDepot).toMatchSnapshot();
        expect(requestDepot.id).toEqual(id);
    });
});