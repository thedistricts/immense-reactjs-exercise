const DEPOTS_DATA = [
    {
        'id': 'D0:52:0',
        'isElectricPublic': false,
        'isElectricFast': false,
        'isDepot': true,
        'isElectricRapidDC': false,
        'isFuelPetrol': true,
        'latitude': '52.04309',
        'bays': '6',
        'isFuelDiesel': false,
        'stationName': 'D0',
        'isElectricResidential': false,
        'longitude': '-0.71796',
    },
    {
        'id': 'D1:52:0',
        'isElectricPublic': false,
        'isElectricFast': false,
        'isDepot': true,
        'isElectricRapidDC': false,
        'isFuelPetrol': false,
        'latitude': '52.00912',
        'bays': '6',
        'isFuelDiesel': true,
        'stationName': 'D1',
        'isElectricResidential': false,
        'longitude': '-0.7426',
    },
    {
        'id': 'D2:52:0',
        'isElectricPublic': false,
        'isElectricFast': true,
        'isDepot': true,
        'isElectricRapidDC': false,
        'isFuelPetrol': false,
        'latitude': '52.03252',
        'bays': '6',
        'isFuelDiesel': false,
        'stationName': 'D2',
        'isElectricResidential': false,
        'longitude': '-0.76528',
    },
];

const DEFAULT = {
    COLOUR: '#0054ff',
};

const getDepotById = (_parent, args) => transformNode({ 
    depot: DEPOTS_DATA.find(d => d.id === args.id),
});

const getDepots = () => transform({ depots: DEPOTS_DATA });

const transform = ({ depots }) => ({
    totalCount: depots.length,
    edges: depots.map(transformEdge),
});

const transformEdge = (depot) => ({
    color: DEFAULT.COLOUR,
    node: transformNode({ depot })
});

const transformNode = ({ depot }) => ({
    id: depot.id,
    name: depot.stationName,
    bays: depot.bays,
    depot: depot.isDepot,
    latitude: depot.latitude,
    longitude: depot.longitude,
    diesel: depot.isFuelDiesel,
    petrol: depot.isFuelPetrol,
    residential: depot.isElectricResidential,
    public: depot.isElectricPublic,
    fast: depot.isElectricFast,
    rapid: depot.isElectricRapidDC,
});

module.exports = {
    getDepots,
    getDepotById,
    transform,
    transformEdge,
    transformNode,
    DEPOTS_DATA,
};