const { resolvers } = require('../');

jest.mock('../resolvers/depots', () => {
    return {
        getDepots: () => ['depots01', 'depots02'],
        getDepotById: () => 'depotXX',
    };
});

describe('graphql | resolvers', () => {
    test('should resolve version ', async () => {
        const dataSources = {
            platformAPI: {
                getVersion: jest.fn().mockResolvedValue({
                    'version': '0.0.1'
                })
            }
        };
        const version = await resolvers.Query.version(null, null, { dataSources })
        expect(version).toBe('0.0.1');
    });
    test('should resolve list of depots', () => {
        expect(resolvers.Query.depots()).toHaveLength(2);
        expect(resolvers.Query.depots()).toMatchSnapshot();
    });
    test('should resolve single depot', () => {
        expect(resolvers.Query.depot()).toBe('depotXX');
    });
});