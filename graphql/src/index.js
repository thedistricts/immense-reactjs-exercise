
const { gql } = require('apollo-server-lambda');
const { getDepots, getDepotById } = require('./resolvers/depots');

const typeDefs = gql`
    type Query {
        version: String
        depots: DepotConnection
        depot(id: ID!): Depot
    }

    # details of a specific depot
    type Depot {
        id: ID!
        name: String
        bays: Int
        depot: Boolean
        latitude: Float
        longitude: Float
        diesel: Boolean
        petrol: Boolean
        residential: Boolean
        public: Boolean
        fast: Boolean
        rapid: Boolean
    }

    # a single depot
    type DepotNode {
        color: String
        node: Depot
    } 

    # list of depots
    type DepotConnection { 
        totalCount: Int!
        edges: [DepotNode]
    }
`;

const resolvers = {
    Query: {
        version: async (_source, _, { dataSources }) => {
            const api = await dataSources.platformAPI.getVersion();
            return api.version;
        },
        depots: getDepots,
        depot: getDepotById,
    },
};

module.exports = {
    typeDefs,
    resolvers,
};