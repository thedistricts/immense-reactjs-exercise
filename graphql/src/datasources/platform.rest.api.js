const { RESTDataSource } = require('apollo-datasource-rest');
class PlatformAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'https://platform.immense.ai/api/v2/';
    }
    async getVersion() {
        return this.get('version');
    }
}
module.exports = PlatformAPI