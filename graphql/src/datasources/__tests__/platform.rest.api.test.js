jest.mock('apollo-datasource-rest', () => {
    class MockRESTDataSource {
        constructor() {
            this.baseUrl = '';
            this.get = jest.fn().mockResolvedValue({
                'version': '20.4.1'
            });
        }
    }
    return {
        RESTDataSource: MockRESTDataSource,
    };
});
const PlatformAPI = require('../platform.rest.api')

describe('datasources | platform', () => {
    test('should initalize PlatformAPI', () => {
        const platformAPI = new PlatformAPI();
        expect(typeof platformAPI.getVersion).toBe('function');
    });

    test('should resolve version data', async () => {
        const platformAPI = new PlatformAPI();
        const api = await platformAPI.getVersion()
        expect(api.version).toBe('20.4.1');
    });
});
